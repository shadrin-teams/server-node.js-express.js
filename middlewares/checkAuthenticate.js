var passport = require("passport");

passport.authenticate('local', {
    successRedirect: '/logged',
    failureRedirect: '/unauthorized'
})