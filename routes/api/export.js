let express = require('express'),
    router = express.Router(),
    config = require('../../config/main'),
    passport = require('passport'),
    fs = require("fs"),
    queue = require('queue'),
    iconv = require('iconv-lite');

let moduleKey = 'export';

router.get('/' + moduleKey + '/:cat', passport.authenticate('jwt', {session: false}), function (req, res, next) {

    let cat = req.params.cat;
    switch (cat) {
        case 'fridges' :
            modelName = 'Productsfridges';
            break;
        case 'washings' :
            modelName = 'Productswashings';
            break;
        case 'conditioners' :
            modelName = 'Productsconditioners';
            break;
        case 'hobs' :
            modelName = 'Productshobs';
            break;
        case 'ovens' :
            modelName = 'Productsovens';
            break;
        default :
            modelName = 'Productsfridges';
    }
    let model = require('../../models/' + modelName);

    let data = '';

    if (config.fields && config.fields[cat]) {

        let listColumns = config.fields[cat];

        let q = queue({concurrency: 1});

        q.push(function (next) {

            let list = model.find({})
                .then(function (list) {

                        for (let i = 0; i < listColumns.length; i++) {
                        data += listColumns[i] + ';';
                    }
                    data += "\n";

                    /*for (let n = 0; n < list.length; n++) {
                        let insideData = '';
                        for (let i = 0; i < listColumns.length; i++) {
                            if (listColumns[i]) {
                                if (list[n][listColumns[i]] && list[n][listColumns[i]] != 'undefined'){
                                    insideData += iconv.encode(list[n][listColumns[i]], 'utf-8' ) + ';';
                                }
                                else insideData += ';';
                            }
                        }
                        if (insideData)data += insideData + "\n";

                    }*/

                    next();
                    console.log(data);

                }, function (error) {
                    next();
                })

        });

        q.start(function (err) {

            fs.writeFile('./public/upload/csv/' + cat + '.csv', data, 'utf8', 0644, 'w+', function (err, file_handle) {
                if (err) {
                    res.send({status: 'error', error: err});
                } else {
                    res.send({status: 'ok', file: 'upload/csv/' + cat + '.csv'});
                }
            });

        });

    } else res.send({status: 'error', error: 'No list of fields for category'});

});

module.exports = router;