let express = require("express"),
    router = express.Router(),
    index = require("./site/index");

module.exports = []
    .concat(router)
    .concat(index);