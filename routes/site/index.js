let express = require("express"),
    router = express.Router();

router.get('/', (req, res, next) => {
    res.render("site/index");
});

module.exports = router;