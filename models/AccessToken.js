var mongoose = require('mongoose');
var crypto = require('crypto');


// AccessToken
var AccessToken = new mongoose.Schema({
    adminId: {
        type: String,
        required: true,
        unique: true
    },
    token: {
        type: String,
        unique: true,
        required: true
    },
    created: {
        type: Date,
        required: true,
        default: Date.now
    },
    date: {
        type: Date,
        required: true,
        default: Date.now
    },
    ip: {
        type: String,
        required: true
    }
});

module.exports = mongoose.model('access_tokens', AccessToken);