var mongoose = require('mongoose');
var crypto = require('crypto');

// User
var Admins = new mongoose.Schema({
    id: {
        type: Number,
        unique: true,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    salt: {
        type: String,
        required: true
    },
    role: {
        type: String
    }
});


Admins.methods.encryptPassword = function(password) {
    return crypto.createHmac('sha1', this.salt).update(password).digest('hex');
};

Admins.methods.verifyPassword = function(password) {
    //console.log('verifyPassword', this.password, this.encryptPassword( password ) );
    if( this.password != this.encryptPassword( password ) ) return false;
    else return true;
};


module.exports = mongoose.model('Admins', Admins);