let mongoose = require('mongoose');

let schema = new mongoose.Schema({
    id: {type: String, required: true, unique: true},
    name: {type: String, required: true},
    site: {type: String, required: true},
    section: {type: String, required: true},
    value: {type: Number},
});

let Sections = mongoose.model('keywords', schema);

module.exports = Sections;